import './style.css';
import * as THREE from 'three'; 
import {
	BoxGeometry,
	BufferGeometry,
	CircleGeometry,
	Color,
	ConeGeometry,
	Curve,
	CylinderGeometry,
	DodecahedronGeometry,
	DoubleSide,
	ExtrudeGeometry,
	Float32BufferAttribute,
	Group,
	IcosahedronGeometry,
	LatheGeometry,
	LineSegments,
	LineBasicMaterial,
	Mesh,
	MeshPhongMaterial,
	OctahedronGeometry,
	PerspectiveCamera,
	PlaneGeometry,
	PointLight,
	RingGeometry,
	Scene,
	Shape,
	ShapeGeometry,
	SphereGeometry,
	TetrahedronGeometry,
	TorusGeometry,
	TorusKnotGeometry,
	TubeGeometry,
	Vector2,
	Vector3,
	WireframeGeometry,
	WebGLRenderer
} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { RGBELoader } from  'three/examples/jsm/loaders/RGBELoader.js';
import { RoundedBoxGeometry } from 'three/examples/jsm/geometries/RoundedBoxGeometry';
import { InteractionManager } from 'three.interactive';
import { SVGLoader } from 'three/examples/jsm/loaders/SVGLoader.js'; 
const scene = new Scene();


const camera = new PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
camera.position.z = 6;

const renderer = new WebGLRenderer( { antialias: true, canvas: document.querySelector('canvas'),alpha: true } );
renderer.setPixelRatio( window.devicePixelRatio / 1.2);
renderer.setSize( window.innerWidth, window.innerHeight );

const pmremGenerator = new THREE.PMREMGenerator( renderer );
new RGBELoader().load( 'environment.hdr', function ( texture ) {

	texture.mapping = THREE.EquirectangularReflectionMapping;

	scene.environment = texture;
    scene.background = texture;
	console.log(scene);
});

const interactionManager = new InteractionManager(
	renderer,
	camera,
	renderer.domElement
  );

const orbit = new OrbitControls( camera, renderer.domElement );


function update(){

	
	
	radiobtn.addEventListener('mouseover', (event) =>{
	
		document.body.style.cursor = 'pointer';
   });

   radiobtn.addEventListener('mouseout', (event) =>{
	
	document.body.style.cursor = 'default';
});


radiobtn.addEventListener('click', (event) =>{
	
group.scale.set( 1.0, 1.0, 1.0);
});

radiobtn.addEventListener('mousedown', (event) =>{
	
	group.scale.set(0.9, 0.9, 0.9);
	});
		

	};




let tick;
let clicked = 1;

const radiogeometry = new RoundedBoxGeometry(2, 2, 0.8);
const radioMaterial = new THREE.MeshStandardMaterial({
  color: 'white'
});

const shape1 = new RoundedBoxGeometry(2, 2, 0.8);

const radiobtn = new THREE.Mesh(radiogeometry,radioMaterial);
scene.add(radiobtn)
	interactionManager.add(radiobtn);


	const tickmaterial = new THREE.MeshStandardMaterial({ color: "white",visible: false });

const renderSVG = (extrusion, svg) => {
  const loader = new SVGLoader();
  const svgData = loader.parse(svg);
  const svgGroup = new THREE.Group();
  const updateMap = [];

  svgGroup.scale.y *= -1;
  svgData.paths.forEach((path) => {
    const shapes = SVGLoader.createShapes(path);

    shapes.forEach((shape) => {
      const meshGeometry = new THREE.ExtrudeBufferGeometry(shape, {
        depth: extrusion,
        bevelEnabled: false,
      });

      const mesh = new THREE.Mesh(meshGeometry, tickmaterial);


      updateMap.push({ shape, mesh });
      svgGroup.add(mesh);
    });
  });

  const box = new THREE.Box3().setFromObject(svgGroup);
  const size = box.getSize(new THREE.Vector3());
  const yOffset = size.y / -2;
  const xOffset = size.x / -2;

  // Offset all of group's elements, to center them
  svgGroup.children.forEach((item) => {
    item.position.x = xOffset;
    item.position.y = yOffset;
  });
 

  return {
    object: svgGroup,
    update(extrusion) {
      updateMap.forEach((updateDetails) => {
        const meshGeometry = new THREE.ExtrudeBufferGeometry(
          updateDetails.shape,
          {
            depth: extrusion,
            bevelEnabled: false,
          }
        );
        const linesGeometry = new THREE.EdgesGeometry(meshGeometry);

        updateDetails.mesh.geometry.dispose();
        updateDetails.lines.geometry.dispose();
        updateDetails.mesh.geometry = meshGeometry;
        updateDetails.lines.geometry = linesGeometry;
      });
    },
  };
};
// scene.js
// svg.js
const svg = `<svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" height="100%" style="fill-rule:nonzero;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;" xml:space="preserve" width="100%" version="1.1" viewBox="0 0 24 24">
<defs/>
<g id="Untitled">
<path d="M438.6 105.4C451.1 117.9 451.1 138.1 438.6 150.6L182.6 406.6C170.1 419.1 149.9 419.1 137.4 406.6L9.372 278.6C-3.124 266.1-3.124 245.9 9.372 233.4C21.87 220.9 42.13 220.9 54.63 233.4L159.1 338.7L393.4 105.4C405.9 92.88 426.1 92.88 438.6 105.4H438.6z" opacity="1" fill="#000000"/>
</g>
</svg>`;
// main.js


const defaultExtrusion = 35;
const { object } = renderSVG(defaultExtrusion, svg);

scene.add(object);
object.scale.set(0.0025,0.0025,0.0025)
object.position.set(0,0.2,0.5)

object.rotation.x = Math.PI / 1;


const group = new THREE.Group();
group.add( radiobtn );
group.add( object );

scene.add( group );


	radiobtn.addEventListener("click", (event) => {
     
	 if(clicked == 1){
		 
		  clicked = 0;
		  radiobtn.material.color.set('#174CF4');
		  tickmaterial.visible = true;
	 }

     else if(clicked == 0){
	
		clicked = 1;
		radiobtn.material.color.set('white')
		tickmaterial.visible = false;
	 }
		
	});


function render() {

	requestAnimationFrame( render );


update()
	renderer.render( scene, camera );

}



window.addEventListener( 'resize', function () {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );

}, false );

render();




